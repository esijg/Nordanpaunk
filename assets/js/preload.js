var images = [];
function preload() {
    for (i = 0; i < arguments.length; i++) {
        images[i] = new Image();
        images[i].src = preload.arguments[i];
    }
}

//-- usage --//
preload(
    "assets/img/nav-bands-down.png",
    "assets/img/nav-faq-down.png",
    "assets/img/nav-location-down.png",
    "assets/img/nav-news-down.png",
    "assets/img/nav-tickets-down.png",
    "assets/img/nav-vision-down.png",
    "assets/img/nav-skull-down.png"
)
